package com.template.spark

import org.apache.spark.sql.DataFrame

import scala.language.implicitConversions

package object utils {

  implicit class DataFrameUtils(df: DataFrame) {

    def camelCase: DataFrame = {
      df.columns.foldLeft(df) { (memoDF, colName) =>
        memoDF.withColumnRenamed(colName, toCamelCase(colName))
      }
    }

    def sneakCase: DataFrame = {
      df.columns.foldLeft(df) { (memoDF, colName) =>
        memoDF.withColumnRenamed(colName, toSneakCase(colName))
      }
    }

  }

  def toCamelCase(name: String): String = {
    "_([a-z\\d])".r.replaceAllIn(name, { m => m.group(1).toUpperCase() })
  }

  def toSneakCase(name: String): String = {
    "[A-Z\\d]".r.replaceAllIn(name, { m => "_" + m.group(0).toLowerCase() })
  }

  implicit class When[T](a: T) {
    def whenever(f: T => Boolean)(g: T => T): T = {
      if(f(a)) g(a) else a
    }
  }

  implicit def when[A](a: A): When[A] = new When[A](a)

}
