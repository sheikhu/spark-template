package com.template.spark.jobs

import com.template.spark.config.{AppConfig, InputParser}
import com.template.spark.core.BaseWorker
import com.template.spark.io.IOHandler
import com.template.spark.io.writer.CsvWriter
import com.template.spark.models.{JobParameters, _}
import com.template.spark.utils._
import org.apache.log4j.Logger
import org.apache.spark.api.java.function.MapFunction
import org.apache.spark.sql.execution.aggregate.{TypedCount, TypedSumDouble, TypedSumLong}
import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.expressions.scalalang._
import org.apache.spark.sql.functions.count
import org.apache.spark.sql.{Encoders, SparkSession}


class EmployeesExtractor(ioHandler: IOHandler, params: JobParameters, autoclose: Boolean = true)
  extends BaseWorker(ioHandler) {

  protected val log: Logger = Logger.getLogger(getClass.getName)

  override def run(options: JobParameters): Unit = {

    val spark = ioHandler.sparkSession
    import spark.implicits._

    // spark.sparkContext.hadoopConfiguration.set("fs.s3a.access.key", sys.env("AWS_ACCESS_KEY"))
    //spark.sparkContext.hadoopConfiguration.set("fs.s3a.secret.key", sys.env("AWS_SECRET_KEY"))
    //spark.sparkContext.hadoopConfiguration.set("fs.s3n.endpoint", "s3.amazonaws.com")


    val employeeSchema = Encoders.product[Employee].schema
    val employeesDf = ioHandler.load(options.input, options.inputFormat, schema = employeeSchema)
      .camelCase
      .withColumn("empNo", $"empNo".cast("long"))
      .as[Employee]

    employeesDf.show

    //new CsvWriter("overwrite", 1).write(employeesDf, params.output)

    if (autoclose) spark.stop
    else log.warn("Spark Session is not closed ! You should close it yourself.")

  }
}

object EmployeesExtractor extends App {

  val params = new InputParser(args).parse

  params match {
    case Some(options) =>

      val spark = SparkSession.builder()
        .appName("EmployeesWorker")
        .getOrCreate()

      spark.sparkContext.setLogLevel("WARN")
      val ioHandler = new IOHandler(spark, new AppConfig)

      new EmployeesExtractor(ioHandler, options).run(options)

    case _ => sys.exit(-1)
  }
}
