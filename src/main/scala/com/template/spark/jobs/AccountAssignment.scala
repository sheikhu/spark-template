package com.template.spark.jobs
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.expressions.scalalang.typed

object AccountAssignment extends App {

  //Create a spark context, using a local master so Spark runs on the local machine
  val spark = SparkSession.builder().master("local[*]").appName("AccountAssignment").getOrCreate()

  //importing spark implicits allows functions such as dataframe.as[T]

  import spark.implicits._

  spark
    .sparkContext
    .parallelize(List(0,1,2,2))
    .toDS()
    .distinct()
    .foreach(n => println(n))

}
