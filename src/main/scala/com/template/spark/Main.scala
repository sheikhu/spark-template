package com.template.spark

import com.template.spark.config.{AppConfig, InputParser}
import com.template.spark.io.IOHandler
import com.template.spark.models.Employee
import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object Main extends App {

  val spark = SparkSession.builder()
    .appName("AppName")
    .enableHiveSupport()
    .getOrCreate()




  spark.stop
}
